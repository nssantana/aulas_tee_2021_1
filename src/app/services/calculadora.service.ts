import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(dividendo, divisor): number | string {
      if(divisor===0){
        return 'não existe divisão por 0'
      }
      return dividendo / divisor;
  }
}
