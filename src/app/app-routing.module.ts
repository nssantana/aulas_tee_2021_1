import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', 
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home-routing.module').then( m => m.HomePageRoutingModule)
  },
  {
    path: 'auth',
    loadChildren: './pages/auth/auth.module#AuthModule'},
    {
      path: 'contas',
      loadChildren: './pages/contas/contas.module#ContasModule'},
  {
    path: 'pagar',
    loadChildren: () => import('./pages/contas/pagar/pagar.module').then( m => m.PagarPageModule)
  },  {
    path: 'calculadora',
    loadChildren: () => import('./pages/calculadora/calculadora.module').then( m => m.CalculadoraPageModule)
  },

];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
