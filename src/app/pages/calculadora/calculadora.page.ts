import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CalculadoraService } from 'src/app/services/calculadora.service';

@Component({
  selector: 'calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  titulo = 'Calculadora';
  form: FormGroup;
  quociente: string | number = 555;

  constructor(
    private builder: FormBuilder,
    private calculadora: CalculadoraService,
    
  ) { }

  ngOnInit() {
    this.form = this.builder.group({
      dividendo: ['',[Validators.required]] ,
      divisor: ['',[Validators.required]],
    });
  }

  dividir() {
    const data = this.form.value; 
    const dividendo = data.dividendo;
    const divisor = data.divisor;

    this.quociente = this.calculadora.divide(dividendo, divisor);
  }

}
