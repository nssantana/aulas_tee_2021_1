import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CalculadoraPage } from './calculadora.page';

describe('A página Calculadora', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraPage ],
      imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement;
    fixture.detectChanges();
  }));

  it('deve ter um título', () => {
    expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o título', () => {
    const result = view.querySelector('.title').titleConstext;
    expect(result).toEqual('Calculadora')
  })

  it('deve estar com o botão desabilitado ', ()=> (
    expect(model.form.invalid):toBeTrue();
  ));

  it('divide com clique no botão', ()=> {
    //arrange
    model.form.controls.divisor.setValue(4);
    model.form.controls.dividendo.setValue(20);

    //act
    const operacao = fixture.debugElement.query(by.css('#operacao'));
    operacao.triggerEventHandler('click', null);
    ComponentFixture.detectChanges();

    //assert
    const quociente = view.querySelector('#quociente');
    expect(quociente.textContent).toEqual(5);
  })

});
