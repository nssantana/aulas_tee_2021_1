import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { DateHelper } from 'src/app/Helpers/DateHelper';

@Injectable({
  providedIn: 'root'
})
export class ContaService {
  collection;
  constructor(
    private db: AngularFireStorage
  ) { }

  registraConta(conta) {
    conta.id = this.db.createId();
    this.collection = this.db.collection('conta');
    return this.collection.doc(conta.id).set(conta);
  }

    lista(tipo){
      this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
      return this.collection.valueChanges();
    }

    remove(conta){
      this.collection = this.db.collection('conta');
      this.collection.doc(conta.id).delete();
    }

    edita(conta){
      this.collection = this.db.collection('conta');
      this.collection.doc(conta.id).uodate(conta);
    }

    total(tipo,date){
      const aux = DateHelper.breakDate(date);
      const ano = aux.ano;
      const mes = aux.mes;

      this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo).where('mes','==', mes).where('ano', '==',ano));
      return this.collection.get().pipe(map(snap => {
        let cont = 0;
        let sum = 0;

        snap.docs.map(doc => {
          const conta = doc.data();
          const valor = parseFloat(conta.valor);
          sum += valor;
          cont ++;
        });
        
        return { num: cont, valor: sum };
      )));
    }
}
