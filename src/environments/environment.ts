// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDj5ByShUDlScD5LNibU_9X64k6Di7jGHU",
    authDomain: "controle-gu3005429.firebaseapp.com",
    projectId: "controle-gu3005429",
    storageBucket: "controle-gu3005429.appspot.com",
    messagingSenderId: "347280554183",
    appId: "1:347280554183:web:d9ee0f4e2596039e9ca6e2",
    measurementId: "G-EW9596L5R4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
